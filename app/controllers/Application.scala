package controllers

import play.api._
import play.api.mvc._
import play.api.data._
import play.api.data.Forms._
import play.api.data.validation.Constraints._

import models._

object Application extends Controller {
  
  val userForm = Form(
    mapping(
      "username" -> nonEmptyText
    )(User.apply)(User.unapply)
  )
  
  val commentForm = Form(
    "text" -> text.verifying(nonEmpty)
  )
  
  val fakeConversation: Comment =
      TextComment(1, User("mark"), "Let's go out tonight!", replies = 
        Seq(
          TextComment(2, User("susan"), "Tea?", replies =
            Seq(
              TextComment(3, User("mark"), "Yeah! It's a good idea."),
              TextComment(4, User("alan"), "What's that?! I want beer!")
            )
          ),
          TextComment(5, User("george"), "Let's go to Scottish Pub.", replies =
            Seq(
              TextComment(6, User("mark"), "Again?!... Let's try something new.", replies =
                Seq(
                  TextComment(7, User("john"), "I'm in for this. Let's go to Clarke Quay.")
                )
              )
            )
          )
        )
      )
  
  def index = Action { implicit req =>
    val username = session.get("username")
    Ok(views.html.index(username.map(User(_)), userForm, models.CommentsRepo.conversations))
  }
  
  def login = Action { implicit req =>
    userForm.bindFromRequest.fold(
      formWithErrors =>
        BadRequest(views.html.index(None, formWithErrors, Seq())),
      user => {
        Redirect(routes.Application.index).withSession(
          session + ("username" -> user.username)
        )
      }
    )
  }
  
  def logout = Action { implicit req =>
    Redirect(routes.Application.index).withSession(
      session - "username"
    )
  }
  
  def getConversation(id: Long) = Action {
    val conversation = CommentsRepo.conversation(id)
    
    conversation match {
      case Some(c) => Ok(views.html.conversation(c))
      case None => BadRequest(s"Conversation with id $id does not exist!")
    }
  }
  
  def addCommentPage(parentId: Option[Long], conversationId: Option[Long]) = Action {
    Ok(views.html.addComment(commentForm, parentId, conversationId))
  }
  
  def addComment(parentId: Option[Long], conversationId: Option[Long]) = Action { implicit req =>
    commentForm.bindFromRequest.fold(
      formWithErrors =>
        BadRequest(views.html.addComment(commentForm, parentId, conversationId)),
      text => {
        session.get("username").map { username =>
          val insertedId = CommentsRepo.addComment(User(username), text, parentId)
          val insertedAnchor = insertedId.map("#" + _).getOrElse("")
          conversationId match {
            case Some(id) => Redirect(routes.Application.getConversation(id) + insertedAnchor)
            case None => Redirect(routes.Application.index)
          }
        }.getOrElse {
          BadRequest("You need to log in!")
        }
      }
    )
  }
}
