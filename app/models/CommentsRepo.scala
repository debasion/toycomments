package models

import anorm._
import anorm.SqlParser._
import play.api.db.DB
import play.api.Play.current
import org.joda.time.DateTime
import org.joda.time.format.DateTimeFormatter
import org.joda.time.format.DateTimeFormat

object CommentsRepo {

  val dateFormatGeneration: DateTimeFormatter = DateTimeFormat.forPattern("yyyyMMddHHmmssSS")
  implicit def rowToDateTime: Column[DateTime] = Column.nonNull { (value, meta) =>
    val MetaDataItem(qualified, nullable, clazz) = meta
    value match {
      case ts: java.sql.Timestamp => Right(new DateTime(ts.getTime))
      case d: java.sql.Date => Right(new DateTime(d.getTime))
      case str: java.lang.String => Right(dateFormatGeneration.parseDateTime(str))
      case _ => Left(TypeDoesNotMatch("Cannot convert " + value + ":" + value.asInstanceOf[AnyRef].getClass))
    }
  }
  implicit val dateTimeToStatement = new ToStatement[DateTime] {
    def set(s: java.sql.PreparedStatement, index: Int, aValue: DateTime): Unit = {
      s.setTimestamp(index, new java.sql.Timestamp(aValue.withMillisOfSecond(0).getMillis()))
    }
  }
  
  val textCommentParser = long("id")~str("username")~str("text")~
      get[DateTime]("creation_time") map {
    case id~username~text~time => TextComment(id, User(username), text, time, Seq())
  }
  
  val commentRowParser = long("id")~str("username")~str("text")~
      get[DateTime]("creation_time")~get[Option[Long]]("parent_id") map {
    case id~username~text~time~parentId =>
      CommentRow(id, User(username), text, time, parentId)
  }
  
  def conversation(id: Long): Option[TextComment] = DB.withConnection { implicit conn =>
    val commentRows = SQL("""
        WITH RECURSIVE tree(id, username, text, creation_time, parent_id) AS (
            SELECT c.id, c.username, c.text, c.creation_time, c.parent_id
            FROM comments c
            WHERE parent_id IS NULL AND id = {id}
          UNION ALL
            SELECT c.id, c.username, c.text, c.creation_time, c.parent_id
            FROM comments c, tree t
            WHERE c.parent_id = t.id
        )
        SELECT * FROM tree
        """)
      .on('id -> id)
      .as(commentRowParser.*)
    val parentGroups = commentRows.groupBy(_.parentId).withDefaultValue(Seq())
    
    def construct(commentRow: CommentRow): TextComment = commentRow match {
      case CommentRow(id, user, text, time, parentId) => {
        val replies = parentGroups(Some(id)).map { row =>
          construct(row)
        }
        TextComment(id, user, text, time, replies)
      }
    }
    
    val conversationRow = commentRows.headOption
    conversationRow.map(construct)
  }
  
  def conversations: Seq[Comment] = DB.withConnection { implicit conn =>
    SQL("SELECT * FROM comments WHERE parent_id IS NULL").as(textCommentParser.*)
  }
  
  def addComment(user: User, text: String, parentId: Option[Long]): Option[Long] =
      DB.withConnection { implicit conn =>
    SQL("""
        INSERT INTO comments 
        VALUES (DEFAULT, {username}, {text}, NOW(), {parent_id})
        """)
      .on("username" -> user.username, "text" -> text, "parent_id" -> parentId)
      .executeInsert()
  }
}

case class CommentRow(
    id: Long,
    user: User,
    text: String,
    creationTime: DateTime,
    parentId: Option[Long])
