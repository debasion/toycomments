package models

import org.joda.time.DateTime

sealed abstract class Comment

case object EmptyComment extends Comment

case class TextComment(
    id: Long,
    user: User,
    text: String,
    time: DateTime = DateTime.now,
    replies: Seq[Comment] = Seq())
  extends Comment
