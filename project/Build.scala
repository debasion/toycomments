import sbt._
import Keys._
import play.Project._

object ApplicationBuild extends Build {

  val appName         = "toycomments"
  val appVersion      = "1.0-SNAPSHOT"

  val appDependencies = Seq(
    // Add your project dependencies here,
    jdbc,
    anorm,
    "postgresql" % "postgresql" % "9.1-901.jdbc4",

    // Joda Time
    "joda-time" % "joda-time" % "2.2",
    "org.joda" % "joda-convert" % "1.3.1",
    "com.github.nscala-time" %% "nscala-time" % "0.4.0"
  )


  val main = play.Project(appName, appVersion, appDependencies).settings(
    // Add your own project settings here      
    scalaVersion := "2.10.2",
    scalacOptions ++= Seq("-feature", "-unchecked", "-deprecation")
  )

}
