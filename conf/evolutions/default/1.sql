# Comments schema

# --- !Ups

CREATE TABLE comments (
  id serial NOT NULL,
  username varchar(32) NOT NULL,
  text varchar(256) NOT NULL,
  creation_time timestamp with time zone NOT NULL,
  parent_id integer,
  CONSTRAINT id PRIMARY KEY (id)
);

# --- !Downs

DROP TABLE comments;
