package models

import org.joda.time.DateTime

object RowParse {
  type RowType = (Long, String, String, DateTime, Option[Long])
  
  val row = Seq(
    (0L, "boss", "?", DateTime.now, None),
    (1L, "calin", "a", DateTime.now, Some(0L)),
    (2L, "irina", "aa", DateTime.now, Some(1L)),
    (3L, "irina", "ab", DateTime.now, Some(1L)),
    (4L, "calin", "b", DateTime.now, Some(0l)),
    (5L, "irina", "ba", DateTime.now, Some(4L))
  )                                               //> row  : Seq[(Long, String, String, org.joda.time.DateTime, Option[Long])] = L
                                                  //| ist((0,boss,?,2013-06-20T00:10:15.071+03:00,None), (1,calin,a,2013-06-20T00:
                                                  //| 10:15.111+03:00,Some(0)), (2,irina,aa,2013-06-20T00:10:15.111+03:00,Some(1))
                                                  //| , (3,irina,ab,2013-06-20T00:10:15.111+03:00,Some(1)), (4,calin,b,2013-06-20T
                                                  //| 00:10:15.111+03:00,Some(0)), (5,irina,ba,2013-06-20T00:10:15.111+03:00,Some(
                                                  //| 4)))
  val gi = row.groupBy(_._1)                      //> gi  : scala.collection.immutable.Map[Long,Seq[(Long, String, String, org.jod
                                                  //| a.time.DateTime, Option[Long])]] = Map(0 -> List((0,boss,?,2013-06-20T00:10:
                                                  //| 15.071+03:00,None)), 5 -> List((5,irina,ba,2013-06-20T00:10:15.111+03:00,Som
                                                  //| e(4))), 1 -> List((1,calin,a,2013-06-20T00:10:15.111+03:00,Some(0))), 2 -> L
                                                  //| ist((2,irina,aa,2013-06-20T00:10:15.111+03:00,Some(1))), 3 -> List((3,irina,
                                                  //| ab,2013-06-20T00:10:15.111+03:00,Some(1))), 4 -> List((4,calin,b,2013-06-20T
                                                  //| 00:10:15.111+03:00,Some(0))))
  val gp = row.groupBy(_._5).withDefaultValue(Seq())
                                                  //> gp  : scala.collection.immutable.Map[Option[Long],Seq[(Long, String, String,
                                                  //|  org.joda.time.DateTime, Option[Long])]] = Map(Some(4) -> List((5,irina,ba,2
                                                  //| 013-06-20T00:10:15.111+03:00,Some(4))), Some(1) -> List((2,irina,aa,2013-06-
                                                  //| 20T00:10:15.111+03:00,Some(1)), (3,irina,ab,2013-06-20T00:10:15.111+03:00,So
                                                  //| me(1))), Some(0) -> List((1,calin,a,2013-06-20T00:10:15.111+03:00,Some(0)), 
                                                  //| (4,calin,b,2013-06-20T00:10:15.111+03:00,Some(0))), None -> List((0,boss,?,2
                                                  //| 013-06-20T00:10:15.071+03:00,None)))
  
  def f(xs: (Long, String, String, DateTime, Option[Long])): Comment = xs match {
    case (id: Long, username: String, text: String, time: DateTime, parentId: Option[_]) =>
      TextComment(id, User(username), text, time, gp(Some(id)).map { ys: RowType =>
        f(ys)
      })
  }                                               //> f: (xs: (Long, String, String, org.joda.time.DateTime, Option[Long]))models.
                                                  //| Comment
  
  //gp(Some(2L))
  f((0L, "boss", "?", DateTime.now, None))        //> res0: models.Comment = TextComment(0,User(boss),?,2013-06-20T00:10:15.187+03
                                                  //| :00,List(TextComment(1,User(calin),a,2013-06-20T00:10:15.111+03:00,List(Text
                                                  //| Comment(2,User(irina),aa,2013-06-20T00:10:15.111+03:00,List()), TextComment(
                                                  //| 3,User(irina),ab,2013-06-20T00:10:15.111+03:00,List()))), TextComment(4,User
                                                  //| (calin),b,2013-06-20T00:10:15.111+03:00,List(TextComment(5,User(irina),ba,20
                                                  //| 13-06-20T00:10:15.111+03:00,List())))))
}