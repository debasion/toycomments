﻿WITH RECURSIVE tree(id, username, text, parent_id) AS (
    SELECT c.id, c.username, c.text, c.parent_id
    FROM comments c
    WHERE parent_id IS NULL
  UNION ALL
    SELECT c.id, c.username, c.text, c.parent_id
    FROM comments c, tree t
    WHERE c.parent_id = t.id
)
SELECT * FROM tree
  